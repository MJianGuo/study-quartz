package com.shadow.job.server.core;

import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.PathResource;

import com.shadow.job.server.util.PathUtil;

/**
 * 加载 properties 文件中的内容到 spring 环境中
 *
 * @author MJianGuo
 */
@Configuration
public class SpringPropertiesLoader extends PropertyPlaceholderConfigurer {

	private static Logger logger = LoggerFactory.getLogger(SpringPropertiesLoader.class);

	public SpringPropertiesLoader() throws URISyntaxException {
		this.load();
	}

	/**
	 * 要加载的配置文件的路径
	 */
	private String[] propertiesFilePathArr = {};

	/**
	 * 加载配置文件到spring 环境中
	 *
	 * @throws URISyntaxException
	 */
	private void load() throws URISyntaxException {

		String currentPath = PathUtil.getCurrentPath();
		logger.info(" currentPath : {}", currentPath);

		List<PathResource> pathResourceList = new ArrayList<>(propertiesFilePathArr.length);
		Path path = null;
		for (String propertiesFilePath : propertiesFilePathArr) {

			logger.info("开始加载 {} 文件到 spring 环境中", propertiesFilePath);
			// 判断是否是 线上环境
			Path onlinePath = Paths.get(currentPath).resolve(propertiesFilePath);
			if (onlinePath.toFile().exists()) {
				path = onlinePath;
			}
			// 判断是否是 本地环境
			URL systemResource = ClassLoader.getSystemResource(propertiesFilePath);
			if (systemResource != null) {
				path = Paths.get(systemResource.toURI());
			}
			if (path == null) {
				logger.error("加载 {} 文件异常...", propertiesFilePath);
				throw new RuntimeException("加载 " + propertiesFilePath + " 异常");
			}
			pathResourceList.add(new PathResource(path));
		}

		this.setLocations(pathResourceList.toArray(new PathResource[] {}));
	}
}