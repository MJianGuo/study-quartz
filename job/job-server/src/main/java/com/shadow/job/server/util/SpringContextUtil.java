package com.shadow.job.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 * Time:2018/7/26 17:18
 *
 * @author MJianGuo
 */
@Component
public class SpringContextUtil implements ApplicationContextAware {

	private static Logger logger = LoggerFactory.getLogger(SpringContextUtil.class);

	private static ApplicationContext context;

	private static Environment environment;

	/**
	 * spring配置文件加载方式
	 *
	 * @param context
	 */
	@Override
	public void setApplicationContext(ApplicationContext context) {
		SpringContextUtil.context = context;
		SpringContextUtil.environment = context.getEnvironment();
		logger.info("设置 ApplicationContext...........................");
	}

	/**
	 * 获取 spring 上下文 ApplicationContext
	 *
	 * @return
	 */
	public static ApplicationContext getApplicationContext() {
		if (context == null) {
			throw new IllegalStateException("context未注入，请在spring配置文件中定义SpringContext或者在应用启动时手动加载");
		}
		return context;
	}

	/**
	 * 根据 bean 的名称，获取spring管理的bean
	 *
	 * @param name
	 *            bean 的名称
	 * @return
	 */
	public static Object getBean(String name) {
		return context.getBean(name);
	}

	/**
	 * 根据 bean 的类型，获取spring管理的bean
	 *
	 * @param clazz
	 *            bean 的类类型
	 * @param <T>
	 * @return
	 */
	public static <T> T getBean(Class<T> clazz) {
		return context.getBean(clazz);
	}

	/**
	 * 根据 bean 的名称和类型，获取spring管理的bean
	 *
	 * @param beanName
	 *            bean的名称
	 * @param clazz
	 *            bean 的类类型
	 * @param <T>
	 * @return
	 */
	public static <T> T getBean(String beanName, Class<T> clazz) {
		return context.getBean(beanName, clazz);
	}

	/**
	 * 根据 key 去 spring 环境中加载对应的值
	 *
	 * @param key
	 *            要获取的值得键
	 * @return
	 */
	public static String getProperty(String key) {
		return environment.getProperty(key);
	}

	/**
	 * 根据 key 去 spring 环境中加载对应的值 ,若不存在则指定默认值
	 *
	 * @param key
	 *            要获取的值得键
	 * @param defaultValue
	 *            默认值
	 * @return
	 */
	public static String getProperty(String key, String defaultValue) {
		return environment.getProperty(key, defaultValue);
	}

	/**
	 * 根据 key 去 spring 环境中加载对应的值 ，并指定返回值类型
	 *
	 * @param key
	 *            要获取的值得键
	 * @param returnType
	 *            返回值类型
	 * @return
	 */
	public static <T> T getProperty(String key, Class<T> returnType) {
		return environment.getProperty(key, returnType);
	}

	/**
	 * 根据 key 去 spring 环境中加载对应的值 ，并指定返回值类型，若没有对应的值则指定默认值
	 *
	 * @param key
	 *            要获取的值得键
	 * @param returnType
	 *            返回值类型
	 * @param defaultValue
	 *            默认值
	 * @return
	 */
	public static <T> T getProperty(String key, Class<T> returnType, T defaultValue) {
		return environment.getProperty(key, returnType, defaultValue);
	}
}
