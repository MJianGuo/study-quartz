package com.shadow.job.server.service.impl;

import java.util.Date;

import org.quartz.CronScheduleBuilder;
import org.quartz.CronTrigger;
import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.TriggerBuilder;
import org.quartz.TriggerKey;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shadow.job.server.bean.AbstractQuartzJob;
import com.shadow.job.server.service.ScheduleJobHandlerService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ScheduleJobHandlerServiceImpl implements ScheduleJobHandlerService {

    @Autowired
    private Scheduler scheduler;

    @Override
    public boolean start() throws SchedulerException {
	if (scheduler.isStarted()) {
	    return true;
	}
	scheduler.start();
	return false;
    }

    @Override
    public boolean shutdown() throws SchedulerException {
	if (scheduler.isShutdown()) {
	    return true;
	}
	scheduler.shutdown(true);
	return false;
    }

    @Override
    public boolean checkExists(String jobName, String jobGroup) throws SchedulerException {
	TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
	return scheduler.checkExists(triggerKey);
    }

    @Override
    public boolean checkExists(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {

	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	return checkExists(jobName, jobGroup);
    }

    @Override
    public boolean addJob(String jobName, String jobGroup, String cronExpression, Job jobBean)
	    throws SchedulerException {

	if (checkExists(jobName, jobGroup)) {
	    log.info(">>>>>>>>> addJob fail, job already exist, jobGroup:{}, jobName:{}", jobGroup, jobName);
	    return false;
	}

	return updateJob(jobName, jobGroup, cronExpression, jobBean);
    }

    @Override
    public boolean addJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {

	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	String cronExpression = abstractQuartzJob.getCronExpression();
	return addJob(jobName, jobGroup, cronExpression, abstractQuartzJob);
    }

    @Override
    public boolean updateJob(String jobName, String jobGroup, String cronExpression, Job jobBean)
	    throws SchedulerException {

	/*
	 * CronTrigger : TriggerKey + cronExpression
	 * withMisfireHandlingInstructionDoNothing 忽略掉调度终止过程中忽略的调度
	 */
	CronScheduleBuilder cronScheduleBuilder = CronScheduleBuilder.cronSchedule(cronExpression)
		.withMisfireHandlingInstructionDoNothing();

	TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
	CronTrigger cronTrigger = TriggerBuilder.newTrigger().withIdentity(triggerKey).withSchedule(cronScheduleBuilder)
		.build();

	JobKey jobKey = JobKey.jobKey(jobName, jobGroup);
	JobDetail jobDetail = JobBuilder.newJob(jobBean.getClass()).withIdentity(jobKey).build();
	jobDetail.getJobDataMap().put(jobName, jobBean);

	Date date = scheduler.scheduleJob(jobDetail, cronTrigger);

	log.info(">>>>>>>>>>> addJob success, jobDetail:{}, cronTrigger:{}, date:{}", jobDetail, cronTrigger, date);
	return true;
    }

    @Override
    public boolean updateJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {

	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	String cronExpression = abstractQuartzJob.getCronExpression();
	return updateJob(jobName, jobGroup, cronExpression, abstractQuartzJob);
    }

    @Override
    public boolean removeJob(String jobName, String jobGroup) throws SchedulerException {

	if (checkExists(jobName, jobGroup)) {
	    TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
	    // 停止触发器
	    scheduler.pauseTrigger(triggerKey);
	    // 移除触发器
	    scheduler.unscheduleJob(triggerKey);
	    // 删除任务
	    scheduler.deleteJob(JobKey.jobKey(jobName, jobGroup));
	}

	return false;
    }

    @Override
    public boolean removeJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {

	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	return removeJob(jobName, jobGroup);
    }

    @Override
    public boolean pauseJob(String jobName, String jobGroup) throws SchedulerException {

	if (checkExists(jobName, jobGroup)) {
	    TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
	    // 停止触发器
	    scheduler.pauseTrigger(triggerKey);
	}

	return false;
    }

    @Override
    public boolean pauseJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {
	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	return pauseJob(jobName, jobGroup);
    }

    @Override
    public boolean resumeJob(String jobName, String jobGroup) throws SchedulerException {
	if (checkExists(jobName, jobGroup)) {
	    TriggerKey triggerKey = TriggerKey.triggerKey(jobName, jobGroup);
	    // 恢复触发器
	    scheduler.resumeTrigger(triggerKey);
	}
	return false;
    }

    @Override
    public boolean resumeJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException {
	String jobName = abstractQuartzJob.getJobName();
	String jobGroup = abstractQuartzJob.getJobGroup();
	return resumeJob(jobName, jobGroup);
    }

}
