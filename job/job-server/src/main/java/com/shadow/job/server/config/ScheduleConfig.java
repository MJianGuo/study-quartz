package com.shadow.job.server.config;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.SchedulerFactoryBean;

import com.shadow.job.server.util.PathUtil;

import lombok.Data;

@Data
@Configuration
@ConfigurationProperties(prefix = "schedule.config")
public class ScheduleConfig {

	/**
	 * quartz.properties 配置文件路径
	 */
	private final String QUARTZ_PROPERTIES_PATH = "config/quartz.properties";

	@Autowired
	private DataSource dataSource;

	private String schedulerName;

	private String applicationContextKey;

	@Bean
	public SchedulerFactoryBean schedulerFactoryBean() {

		SchedulerFactoryBean factory = new SchedulerFactoryBean();
		factory.setDataSource(dataSource);

		factory.setQuartzProperties(PathUtil.loadPropertiesFile(QUARTZ_PROPERTIES_PATH));

		factory.setSchedulerName(schedulerName);
		// 延时启动
		factory.setStartupDelay(30);
		factory.setApplicationContextSchedulerContextKey(applicationContextKey);
		// 可选，QuartzScheduler
		// 启动时更新己存在的Job，这样就不用每次修改 targetObject 后删除 qrtz_job_details 表对应记录了
		factory.setOverwriteExistingJobs(true);
		// 设置自动启动，默认为true
		factory.setAutoStartup(true);

		return factory;
	}

}
