package com.shadow.job.server.service;

import org.quartz.Job;
import org.quartz.SchedulerException;

import com.shadow.job.server.bean.AbstractQuartzJob;

/**
 * 
 * @author MJianGuo
 *
 */
public interface ScheduleJobHandlerService {

    /**
     * 开启任务
     * 
     * @return
     */
    boolean start() throws SchedulerException;

    /**
     * 停止任务
     * 
     * @return
     */
    boolean shutdown() throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称校验该任务是否存在
     * 
     * @param jobName  任务名称
     * @param jobGroup 执行器分组名称
     * @return
     * @throws SchedulerException
     */
    boolean checkExists(String jobName, String jobGroup) throws SchedulerException;

    /**
     * 根据任务名称和任务分组校验该任务是否存在
     * 
     * @param abstractQuartzJob 封装job任务名称和任务分组的bean
     * @return
     * @throws SchedulerException
     */
    boolean checkExists(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称、cron执行表达式添加执行任务
     * 
     * @param jobName        任务名称
     * @param jobGroup       执行器分组名称
     * @param cronExpression cron 任务执行时间表达式
     * @param jobBean        任务执行类
     * @return
     * @throws SchedulerException
     */
    boolean addJob(String jobName, String jobGroup, String cronExpression, Job jobBean) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称、行cron执行表达式添加执行任务
     * 
     * @param abstractQuartzJob 封装job任务名称、执行器分组名称的bean
     * @return
     * @throws SchedulerException
     */
    boolean addJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称、cron执行表达式、执行任务类更新执行任务
     * 
     * @param jobName        任务名称
     * @param jobGroup       执行器分组名称
     * @param cronExpression cron 任务执行时间表达式
     * @param jobBean        任务执行类
     * @return
     * @throws SchedulerException
     */
    boolean updateJob(String jobName, String jobGroup, String cronExpression, Job jobBean) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称、cron执行表达式、执行任务类更新执行任务
     * 
     * @param abstractQuartzJob 封装job任务名称、执行器分组名称的bean
     * @return
     * @throws SchedulerException
     */
    boolean updateJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称移除执行任务
     * 
     * @param jobName  任务名称
     * @param jobGroup 执行器分组名称
     * 
     * @return
     * @throws SchedulerException
     */
    boolean removeJob(String jobName, String jobGroup) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称移除执行任务
     * 
     * @param abstractQuartzJob 封装job任务名称、执行器分组名称的bean
     * @return
     * @throws SchedulerException
     */
    boolean removeJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称暂停执行任务
     * 
     * @param jobName  任务名称
     * @param jobGroup 执行器分组名称
     * 
     * @return
     * @throws SchedulerException
     */
    boolean pauseJob(String jobName, String jobGroup) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称暂停执行任务
     * 
     * @param abstractQuartzJob 封装job任务名称、执行器分组名称的bean
     * @return
     * @throws SchedulerException
     */
    boolean pauseJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称恢复执行任务
     * 
     * @param jobName  任务名称
     * @param jobGroup 执行器分组名称
     * 
     * @return
     * @throws SchedulerException
     */
    boolean resumeJob(String jobName, String jobGroup) throws SchedulerException;

    /**
     * 根据任务名称、执行器分组名称恢复执行任务
     * 
     * @param abstractQuartzJob 封装job任务名称、执行器分组名称的bean
     * @return
     * @throws SchedulerException
     */
    boolean resumeJob(AbstractQuartzJob abstractQuartzJob) throws SchedulerException;
}
