package com.shadow.job.server;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.shadow.job.server.bean.AbstractQuartzJob;
import com.shadow.job.server.service.ScheduleJobHandlerService;
import com.shadow.job.server.util.SpringContextUtil;

@SpringBootApplication
public class Application {

	public static void main(String[] args) throws SchedulerException, InterruptedException {
		SpringApplication.run(Application.class, args);

		AbstractQuartzJob job = new AbstractQuartzJob() {

			@Override
			protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

				System.out.println("==========>>>>>>" + LocalDateTime.now());
			}
		};

		job.setJobName("test");
		job.setJobGroup("testGroup");
		job.setCronExpression("0/5 * * * * ? ");

		ScheduleJobHandlerService bean = SpringContextUtil.getBean(ScheduleJobHandlerService.class);

		bean.addJob(job);
		String property = SpringContextUtil.getProperty("schedule.config.scheduler-name");
		System.out.println("property ===>>>>>>>" + property);
		
		TimeUnit.SECONDS.sleep(60L);
		
		bean.removeJob(job);
	}
}
