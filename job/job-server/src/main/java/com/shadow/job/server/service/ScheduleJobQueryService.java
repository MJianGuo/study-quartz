package com.shadow.job.server.service;

import java.util.List;
import java.util.Set;

import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.TriggerKey;
import org.quartz.Trigger.TriggerState;

public interface ScheduleJobQueryService {

	List<JobExecutionContext> getCurrentlyExecutingJobs() throws SchedulerException;

	List<String> getJobGroupNames() throws SchedulerException;

	List<String> getTriggerGroupNames() throws SchedulerException;

	Set<String> getPausedTriggerGroups() throws SchedulerException;

	List<? extends Trigger> getTriggersOfJob(JobKey jobKey) throws SchedulerException;

	JobDetail getJobDetail(JobKey jobKey) throws SchedulerException;

	Trigger getTrigger(TriggerKey triggerKey) throws SchedulerException;

	TriggerState getTriggerState(TriggerKey triggerKey) throws SchedulerException;

	List<String> getCalendarNames() throws SchedulerException;
}
