package com.shadow.job.server.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.Properties;

/**
 * 文件路径加载工具类
 *
 * @author MJianGuo
 */
public final class PathUtil {

	private static Logger logger = LoggerFactory.getLogger(PathUtil.class);

	private PathUtil() {
	}

	/**
	 * 操作系统类型
	 */
	private enum OsType {
		/**
		 * window
		 */
		WINDOW,
		/**
		 * linux
		 */
		LINUX
	}

	/**
	 * 将配置文件加载到 Properties 集合中
	 * 
	 * @param resourcePath
	 * @return
	 */
	public static Properties loadPropertiesFile(String resourcePath) {
		Path path = getResourcePath(resourcePath);
		Properties properties = new Properties();
		try {
			FileInputStream fileInputStream = new FileInputStream(path.toFile());
			properties.load(fileInputStream);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return properties;
	}

	/**
	 * 加载指定路径的配置文件
	 *
	 * @param resourcePath
	 *            配置文件的路径
	 * @return
	 */
	public static Path getResourcePath(String resourcePath) {

		String currentPath = getCurrentPath();
		logger.info("load file {} , currentPath is {}", resourcePath, currentPath);

		// 判断是否是服务器环境
		// 1.同级目录查询
		Path path = Paths.get(currentPath).resolve(resourcePath);
		if (path.toFile().exists()) {
			return path;
		}

		// 2.父级目录查询
		Path currentParentPath = getCurrentParentPath();
		logger.info("load file {} , currentParentPath is {}", resourcePath, currentParentPath);
		if (!Objects.isNull(currentParentPath)) {
			path = currentParentPath.resolve(resourcePath);
			if (path.toFile().exists()) {
				return path;
			}
		}

		// 判断是否是本地环境
		URL localResourceUrl = ClassLoader.getSystemResource(resourcePath);
		logger.info("load file {} by local.. ", resourcePath);
		if (!Objects.isNull(localResourceUrl)) {
			try {
				path = Paths.get(localResourceUrl.toURI());
				if (path.toFile().exists()) {
					return path;
				}
			} catch (URISyntaxException e) {
				logger.error(e.getMessage(), e);
				logger.error("load {} file is fail. currentPath is {}", resourcePath, currentPath, e);
			}
		}

		return null;
	}

	/**
	 * 获取当前路径的父机目录
	 *
	 * @return
	 */
	public static Path getCurrentParentPath() {
		String currentPath = getCurrentPath();
		Path path = Paths.get(currentPath);
		if (path.toFile().exists()) {
			return path.getParent();
		}
		return null;
	}

	/**
	 * 获取当前jar包所在目录 / 程序bin所在目录
	 *
	 * @return
	 */
	public static String getCurrentPath() {

		String path = System.getProperty("serviceframe.config.path");
		System.out.println("serviceframe.config.path:" + path);
		logger.info("serviceframe.config.path:{}", path);

		if (path == null || "".equalsIgnoreCase(path)) {
			Class<?> caller = getCaller();
			if (caller == null) {
				caller = PathUtil.class;
			}
			path = getCurrentPath(caller);
		}

		logger.info("PathUtil getCurrentPath:{}", path);
		return path;
	}

	private static Class<?> getCaller() {
		StackTraceElement[] stack = (new Throwable()).getStackTrace();
		logger.info("stack length::{}", stack.length);
		if (stack.length < 3) {
			return PathUtil.class;
		}
		String className = stack[2].getClassName();
		logger.info("getCaller class name:{}", className);
		try {
			return Class.forName(className);
		} catch (ClassNotFoundException e) {
			logger.error(e.getMessage(), e);
		}
		return null;
	}

	/**
	 * 获取当前class父目录
	 *
	 * @param cls
	 * @return 当前class父目录 URL
	 */
	public static String getCurrentPath(Class<?> cls) {
		String path = cls.getProtectionDomain().getCodeSource().getLocation().getPath();
		path = path.replaceFirst("file:/", "");
		path = path.replaceAll("!/", "");
		if (path.lastIndexOf(File.separator) >= 0) {
			path = path.substring(0, path.lastIndexOf(File.separator));
		}
		if ("/".equalsIgnoreCase(path.substring(0, 1))) {
			String osName = System.getProperty("os.name").toLowerCase();
			if (osName.indexOf(OsType.WINDOW.name().toLowerCase()) >= 0) {
				path = path.substring(1);
			}
		}
		return path;
	}
}