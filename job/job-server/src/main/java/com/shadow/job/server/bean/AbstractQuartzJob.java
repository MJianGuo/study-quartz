package com.shadow.job.server.bean;

import java.io.Serializable;

import org.springframework.scheduling.quartz.QuartzJobBean;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public abstract class AbstractQuartzJob extends QuartzJobBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2736865380336682558L;

	/**
	 * 任务名称
	 */
	protected String jobName;

	/**
	 * 任务执行的执行器组名称
	 */
	protected String jobGroup;

	/**
	 * 任务执行的 Cron 表达式 * * * * * ?
	 */
	protected String cronExpression;
}
