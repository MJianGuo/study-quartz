package com.shadow.job.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JobAmdinApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobAmdinApplication.class, args);
	}
}
